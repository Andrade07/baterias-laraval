@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Medições</h1>
        <hr>
        <h2>Descrição do medições:</h2>
        
        <table id="tbDados" class="table table-striped table-bordered"> 
            <thead>
                <tr>
                    <th>Pilha/Bateria</th>
                    <th>Tensão Nominal(v)</th>
                    <th>Capacidade da corrente (mA.h)</th>
                    <th>Tensão sem carga(V)</th>
                    <th>Tensão com carga(V)</th>
                    <th>Resistencia de carga(ohm)</th>
                    <th>Resistencia interna(ohm)</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($medicoes as $medicao)
                <tr>
                    <td>{{$medicao->pilha_bateria}}</td>
                    <td>{{number_format($medicao->tensao_nominal,1,'.','')}}</td>
                    <td>{{$medicao->capacidade_corrente}}</td>
                    <td>{{$medicao->tensao_sem_carga}}</td>
                    <td>{{$medicao->tensao_com_carga}}</td>
                    <td>{{$medicao->resistencia_carga}}</td>
                    <td>{{number_format($medicao->resistencia_interna,3,'.','')}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </main>
    <script src="../js/scripts.js"></script>
    @endsection

@section('rodape')
<h4>Rodapé Medições</h4>
@endsection