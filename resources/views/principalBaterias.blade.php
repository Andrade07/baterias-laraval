@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Turma: 2D2 - Grupo 5</h1>
        <h2>Participantes:</h2>
        <hr>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Matricula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0073607</td>
                <td>Bruno Andrade</td>
                <td>Dev Java, HTML e Css</td>
            </tr>
            <tr>
                <td>0072536</td>
                <td>Gabriel Riquelme</td>
                <td>Gerente</td>
            </tr>
            <tr>
                <td>0072534</td>
                <td>Victor Simão</td>
                <td>Medições e desenvolvedor Java</td>
            </tr>
            <tr>
                <td>0072560</td>
                <td>Vinicius Pedrosa</td>
                <td>Medições e desenvolvedor Java</td>
            </tr>
        </table>
        <img class="grupo" src="imgs/grupo.jpeg" alt="Participantes do grupo">
    </main>
    @endsection

@section('rodape')
<h4>Rodapé Principal Baterias</h4>
@endsection