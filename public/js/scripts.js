//alert('ok');
 
//  objeto JSON com os dados
var dados = [
    {
      pilha: "Pilha Alcalina Duracel AA",
      tensao_nominal: 1.5,
      corrente: 2800,
      E: 1.306,
      V: 1.290,
      R: 23.8,
    },
    {
      pilha: "Pilha Alcalina Duracel AAA",
      tensao_nominal: 1.5,
      corrente: 1200,
      E: 0.990,
      V: 0.860,
      R: 23.7,
    },
    {
      pilha: "Luatek",
      tensao_nominal: 3.7,
      corrente: 1200,
      E: 2.498,
      V: 2.403,
      R: 23.7,
    },
    {
      pilha: "Elgin",
      tensao_nominal: 9.0,
      corrente: 250,
      E: 7.27,
      V: 2.080,
      R: 23.8,
    },
    {
        pilha: "Golite",
        tensao_nominal: 9.0,
        corrente: 500,
        E: 5.86,
        V: 0.273,
        R: 23.8,
      },
      {
        pilha: "Panasonic",
        tensao_nominal: 1.5,
        corrente: 2500,
        E: 1.379,
        V: 1.344,
        R: 23.8,
      },
      {
        pilha: "Phillips",
        tensao_nominal: 5.0,
        corrente: 2500,
        E: 1.370,
        V: 1.328,
        R: 23.8,
      },
      {
        pilha: "Jyx",
        tensao_nominal: 3.7,
        corrente: 9800,
        E: 2.599,
        V: 2.530,
        R: 23.8,
      },
      {
        pilha: "UniPower",
        tensao_nominal: 12.0,
        corrente: 7.0,
        E: 10.50,
        V: 10.30,
        R: 23.8,
      },
      {
        pilha: "Freedom",
        tensao_nominal: 12.0,
        corrente: 30,
        E: 10.69,
        V: 10.61,
        R: 23.8,
      },
  ];
   
  // Função para calcular a resistencia interna de cada medição e preencher a tabela
  function calcularResistencia() {
    var tabela = document.getElementById("tbDados");
   
    // Limpar tabela antes de preencher
    var tabelaHTML = `<tr>
                              <th>Pilha/Bateria</th>
                              <th>Tensão nominal (V)</th>
                              <th>Capacidade de corrente (mA.h)</th>
                              <th>Tensão sem carga(V)</th>
                              <th>Tensão com carga(V)</th>
                              <th>Resitência de carga(ohm)</th>
                              <th>Resistência interna(ohm)</th>
                    </tr>`;
   
    // Iterar sobre os dados e calcular o r de cada um
    for (var i = 0; i < dados.length; i++) {
      var linha = dados[i];
      var E = linha.E;
      var V = linha.V;
      var R = linha.R;
      var r = R * (E / V - 1);
   
      // Adicionar nova linha à tabela com os valores e a soma
      var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
   
      tabelaHTML += novaLinha;
    }
    tabela.innerHTML = tabelaHTML;
  }
   
  calcularResistencia();